package com.example.android.booklistingapp;


import java.util.ArrayList;

public interface AsyncResponse {

    void processFinish(ArrayList<String> title, ArrayList<String> author);

}
