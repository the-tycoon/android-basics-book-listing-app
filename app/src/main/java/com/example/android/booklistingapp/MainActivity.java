package com.example.android.booklistingapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    Button button;
    ListView listView;
    ArrayList<Book> books;
    BookAdapter adapter;
    BookAsyncTask bookAsynctask;
    TextView defaultTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initializing all Variable and views.
        editText = (EditText) findViewById(R.id.search_text);
        button = (Button) findViewById(R.id.search_button);
        listView = (ListView) findViewById(R.id.list);
        books = new ArrayList<Book>();
        adapter = new BookAdapter(this, books);
        defaultTextView = (TextView) findViewById(R.id.default_text_view);

        //Setiing on button click
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = createStringUrl();
                if (url != "") {
                    bookAsynctask = new BookAsyncTask(new AsyncResponse() {
                        @Override
                        public void processFinish(ArrayList<String> title, ArrayList<String> author) {
                            clearBookList();
                            if (title.size() != 0) {
                                defaultTextView.setVisibility(View.GONE);
                                listView.setVisibility(View.VISIBLE);
                                for (int i = 0; i < title.size(); i++) {
                                    addBook(title.get(i), author.get(i));
                                }
                            } else {
                                listView.setVisibility(View.GONE);
                                defaultTextView.setVisibility(View.VISIBLE);
                            }
                            listView.setAdapter(adapter);
                        }
                    });
                    bookAsynctask.execute(url);
                }
            }
        });

    }

    /**
     * Clears books ArrayList.
     */
    private void clearBookList() {
        books.clear();
    }

    /**
     * Add title and author to the books ArrayList.
     *
     * @param title
     * @param author
     */
    private void addBook(String title, String author) {
        books.add(new Book(title, author));
    }

    /**
     * Create String url.
     */
    private String createStringUrl() {
        String keyword = "";
        if (!editText.getText().toString().equals("")) {
            keyword = editText.getText().toString();
            Toast.makeText(MainActivity.this, "Searching...", Toast.LENGTH_SHORT).show();
            return "https://www.googleapis.com/books/v1/volumes?" + "q=" + keyword;
        } else {
            Toast.makeText(MainActivity.this, "No keyword has been provided.", Toast.LENGTH_SHORT).show();
        }
        return keyword;
    }

}
